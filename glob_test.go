package glob_test

import (
	"testing"

	"codeberg.org/gruf/go-glob"
	glob2 "github.com/ryanuber/go-glob"
)

var tests = []struct {
	Pattern string
	Check   string
	Expect  bool
}{
	{
		Pattern: "",
		Check:   "",
		Expect:  true,
	},
	{
		Pattern: "",
		Check:   "hello",
		Expect:  false,
	},
	{
		Pattern: "",
		Check:   "*",
		Expect:  false,
	},
	{
		Pattern: "*",
		Check:   "",
		Expect:  true,
	},
	{
		Pattern: "*",
		Check:   "*",
		Expect:  true,
	},
	{
		Pattern: "*",
		Check:   "hello",
		Expect:  true,
	},
	{
		Pattern: "hello",
		Check:   "hello",
		Expect:  true,
	},
	{
		Pattern: "*hello",
		Check:   "hello",
		Expect:  true,
	},
	{
		Pattern: "hello*",
		Check:   "hello",
		Expect:  true,
	},
	{
		Pattern: "hello",
		Check:   "*hello",
		Expect:  false,
	},
	{
		Pattern: "hello",
		Check:   "hello*",
		Expect:  false,
	},
	{
		Pattern: `hello\*`,
		Check:   `hello*`,
		Expect:  true,
	},
	{
		Pattern: `hello\*`,
		Check:   `hello**`,
		Expect:  false,
	},
	{
		Pattern: `hello\*`,
		Check:   `hello\*`,
		Expect:  false,
	},
	{
		Pattern: `hello\*`,
		Check:   `helloaaaaaaaaa`,
		Expect:  false,
	},
	{
		Pattern: `hello\\*`,
		Check:   `hello*`,
		Expect:  false,
	},
	{
		Pattern: `hello\\*`,
		Check:   `hello\\`,
		Expect:  true,
	},
	{
		Pattern: `hello\\*`,
		Check:   `hello\\*`,
		Expect:  true,
	},
}

var testMulti = []struct {
	Patterns []string
	Checks   []struct {
		String string
		Expect bool
	}
}{
	{
		Patterns: []string{
			`/home/*/Downloads`,
			`/home/grufwub/Downloads/*`,
		},
		Checks: []struct {
			String string
			Expect bool
		}{
			{
				String: `/home/grufwub/Downloads/pictures`,
				Expect: true,
			},
			{
				String: `/home/phoebe/Downloads`,
				Expect: true,
			},
			{
				String: `/home/*/Downloads`,
				Expect: true,
			},
		},
	},
}

var precompiled = func() (expr []struct {
	Expr  *glob.GlobExpr
	Check string
}) {
	for _, test := range tests {
		expr = append(expr, struct {
			Expr  *glob.GlobExpr
			Check string
		}{
			Expr:  glob.Compile(test.Pattern),
			Check: test.Check,
		})
	}
	return
}()

func TestMatch(t *testing.T) {
	for _, test := range tests {
		if glob.Match(test.Pattern, test.Check) != test.Expect {
			t.Fatalf("failed asserting expected result: %#v", test)
		}
	}
}

func TestFind(t *testing.T) {
	for _, test := range tests {
		matches := glob.Compile(test.Pattern).Find(test.Check)
		t.Logf("matches: %#v %#v", test, matches)
		if (len(matches) > 0) != test.Expect {
			t.Fatal("failed asserting expected result")
		}
	}
}

func TestMatchMulti(t *testing.T) {
	for _, test := range testMulti {
		expr := glob.CompileMulti(test.Patterns...)
		for _, check := range test.Checks {
			pattern, matches := expr.Find(check.String)
			if (len(matches) > 0) != check.Expect {
				t.Fatalf("failed asserting expected result: %#v", check)
			}
			t.Logf("matched with: %q %#v", pattern, matches)
		}
	}
}

func BenchmarkGlob(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for _, test := range tests {
				glob.Match(test.Pattern, test.Check)
			}
		}
	})
}

func BenchmarkMatchPrecompiled(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for _, test := range precompiled {
				test.Expr.Match(test.Check)
			}
		}
	})
}

func BenchmarkRyanUberGlob(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			for _, test := range tests {
				glob2.Glob(test.Pattern, test.Check)
			}
		}
	})
}
