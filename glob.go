package glob

import (
	"strings"
)

const (
	// GlobExpr flag bit constants.
	leadBit  = uint8(1) << 0
	trailBit = uint8(1) << 1
)

// GlobExpr represents a compiled glob expression.
type GlobExpr struct {
	parts []string
	flags uint8
}

// Match will attempt to match against receiving glob expression.
func (expr *GlobExpr) Match(s string) bool {
	if len(expr.parts) < 1 {
		// Pattern is empty,
		// -> 's' MUST be empty
		if expr.flags&leadBit == 0 {
			return len(s) < 1
		}

		// Pattern is glob,
		// -> matches all
		return true
	}

	cur := 0
	parts := expr.parts

	if expr.flags&leadBit == 0 {
		// No leading glob in pattern

		// 's' MUST start with first part
		idx := strings.Index(s, parts[0])
		if idx != 0 {
			return false
		}

		// Set next cursor pos, skip first
		cur = idx + len(parts[0])
		parts = parts[1:]
	}

	for _, part := range parts {
		// Look for start of next section
		idx := strings.Index(s[cur:], part)
		if idx < 0 {
			return false
		}

		// Set next cursor pos
		cur = idx + len(part)
	}

	if expr.flags&trailBit == 0 {
		// No trailing glob in pattern

		// MUST have reached end
		return (cur == len(s))
	}

	return true
}

// Find will attempt to match against receiving glob expression, returning matching string and globbed sections.
func (expr *GlobExpr) Find(s string) []string {
	matches := []string{s}

	if len(expr.parts) < 1 {
		// Pattern is empty,
		// -> 's' MUST be empty
		if expr.flags&leadBit == 0 &&
			len(s) > 0 {
			return nil
		}

		// Either glob OR 's' = ''
		return matches
	}

	cur, last := 0, 0
	parts := expr.parts

	if expr.flags&leadBit == 0 {
		// No leading glob in pattern

		// 's' MUST start with first part
		idx := strings.Index(s, parts[0])
		if idx != 0 {
			return nil
		}

		// Set next cursor pos, skip first
		cur = idx + len(parts[0])
		parts = parts[1:]
		last = cur
	}

	for _, part := range parts {
		// Look for start of next section
		idx := strings.Index(s[cur:], part)
		if idx < 0 {
			return nil
		}

		// Append glob match
		matches = append(matches, s[last:cur])

		// Set next cursor pos
		last = cur
		cur = idx + len(part)
	}

	if expr.flags&trailBit == 0 {
		// No trailing glob in pattern

		// MUST have reached end
		if cur != len(s) {
			return nil
		}

		return matches
	}

	// Append trailing match
	matches = append(matches, s[cur:])
	return matches
}

// Match will attempt to match against supplied glob pattern.
func Match(pattern string, s string) bool {
	return Compile(pattern).Match(s)
}

// Find will attempt to match against supplied glob pattern, returning matching string and globbed sections.
func Find(pattern string, s string) []string {
	return Compile(pattern).Find(s)
}

// Compile will compile supplied glob pattern into a GlobExpr.
func Compile(pattern string) *GlobExpr {
	var flags uint8
	var parts []string

	switch len(pattern) {
	// No pattern
	case 0:

	// Either single char, or
	// single glob (matches all)
	case 1:
		if pattern[0] == '*' {
			flags |= leadBit
		}

	// Pattern needs decoding
	default:
		b := []rune{}
		delim := false

		// Iterate the pattern for globs
		for i, r := range pattern {
			if r == '*' {
				if delim {
					// Delimited, add as char
					b = append(b, '*')
					delim = false
					continue
				}

				if i == 0 {
					// Leading glob
					flags |= leadBit
					continue
				} else if i == len(pattern)-1 {
					// Trailing glob
					flags |= trailBit
					break
				}

				// Append the next pattern part
				parts = append(parts, string(b))
				b = b[:0]
			} else if r == '\\' && !delim {
				// Toggle delim status
				delim = true
			} else {
				if delim {
					// Delim was char
					b = append(b, '\\')
					delim = false
				}

				// Write to part builder
				b = append(b, r)
			}
		}

		// Append remaining part
		if len(b) > 0 {
			parts = append(parts, string(b))
		}
	}

	return &GlobExpr{
		parts: parts,
		flags: flags,
	}
}
