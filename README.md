# go-glob

glob expression matching library, with both single and optimised (using radix tree) multi matcher

on an i7-10750h CPU the precompiled `GlobExpr.Match()` benches ~35x faster (at ~ `20ns/op`) than the simpler `Match()` function (at ~ `750ns/op`). this is due to the `Match()` function needing to compile the glob expression at call-time, so pre-compiling glob expressions where possible is highly recommended

## todos

- benchmarks

- more tests
