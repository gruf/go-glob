package glob

import (
	"strings"
	"unicode/utf8"
)

// GlobMultiExpr represents a compiled multi glob expression.
type GlobMultiExpr struct {
	root node
}

// Match will attempt to match supplied string against receiving multi glob expression.
func (expr *GlobMultiExpr) Match(s string) bool {
	_, matches := expr.Find(s)
	return (len(matches) > 0)
}

// Find will attempt to find the matching pattern within receiving multi glob expression,
// it returns the pattern matched against and matching string with globbed sections.
func (expr *GlobMultiExpr) Find(s string) (string, []string) {
	return expr.root.Get(s)
}

// CompileMulti will compile multiple glob patterns into a GlobMultiExpr.
func CompileMulti(patterns ...string) *GlobMultiExpr {
	expr := &GlobMultiExpr{}
	for _, pattern := range patterns {
		expr.root.Add(pattern)
	}
	return expr
}

const (
	// node flag bit constants.
	globBit      = uint8(1) << 0
	globChildBit = uint8(1) << 1
)

// node representrs a node in a radix tree.
type node struct {
	prefix   string  // prefix is this node's radix tree prefix
	flags    uint8   // flags stores node bit flags
	children []*node // children is a slice of child nodes of this current node
	pattern  string  // pattern is original input pattern this node can match on
}

// Get attempts a match down the radix tree for given input, returning
// the matching pattern and matching string with globbed sections.
func (n *node) Get(input string) (string, []string) {
	// frame represents a state of
	// iterating down a radix tree
	type frame struct {
		input string
		match []string
		node  *node
		child int
	}

	var (
		// matches is a slice of glob expression matches,
		// with index 0 = whole input string
		matches = []string{input}

		// stack is essentially a call-frame stack, but
		// for iterating down a tree of nodes
		stack []frame

		// lastFrame is a ptr to the last re-entered frame
		lastFrame *frame

		// snapshot attempts to take a frame snapshot and add
		// to stack for given child index. returns false if the
		// the child index is of the last popped frame
		snapshot = func(child int) bool {
			if lastFrame != nil &&
				lastFrame.child <= child {
				lastFrame = nil
				return false
			}
			stack = append(stack, frame{
				input: input,
				match: matches,
				node:  n,
				child: child,
			})
			return true
		}
	)

	for {
	inner:
		for len(input) > 0 {
			switch {
			// This is a glob node
			case n.IsGlob():
				// Check child nodes for matching prefix
				for i, child := range n.children {
					idx := strings.Index(input, child.prefix)
					if idx >= 0 {
						// Attempt to store frame
						if !snapshot(i) {
							continue
						}

						// Skip past the matching child prefix
						matches = append(matches, input[:idx])
						input = input[idx:]
						n = child
						continue inner
					}
				}

				// Check for glob match
				if len(n.pattern) > 0 {
					matches = append(matches, input)
					return n.pattern, matches
				}

			// This is a prefix node
			default:
				// Get child iter length
				length := len(n.children)
				if n.HasGlobChild() {
					length--
				}

				for i := 0; i < length; i++ {
					child := n.children[i]

					// Check child node for matching prefix
					if strings.HasPrefix(input, child.prefix) {
						// Attempt to store frame
						if !snapshot(i) {
							continue
						}

						// Skip past the matching child prefix
						input = input[len(child.prefix):]
						n = child
						continue inner
					}
				}

				if n.HasGlobChild() {
					// Node has a glob child

					// Attempt to snapshot frame
					// and iter down glob child
					if snapshot(-1) {
						n = n.children[length]
						continue inner
					}

					// Erase last frame
					lastFrame = nil
				}

				// Must be an EXACT prefix mode match
				if len(input) == len(n.prefix) {
					if len(n.pattern) > 0 {
						return n.pattern, matches
					}
				}
			}

			// No match
			break inner
		}

		// Climb back up stack
		if len(stack) > 0 {
			i := len(stack) - 1

			// Get the last frame
			lastFrame = &stack[i]

			// Update loop vars
			input = lastFrame.input
			n = lastFrame.node
			matches = lastFrame.match

			// Drop from stack
			stack = stack[:i]
			continue
		}

		// Match failed
		return "", nil
	}
}

// Add will add a pattern match to the radix tree. This will only be called from root.
func (n *node) Add(pattern string) {
	// Keep ptr to original
	original := pattern

loop:
	for len(pattern) > 0 {
		// Find longest common prefix for current node
		i := longestCommonPrefix(pattern, n.prefix)

		// Less than current node prefix,
		// we need to split this node
		if i < len(n.prefix) {
			// End of split, this becomes
			// the state of current node
			child := &node{
				prefix:   n.prefix[i:],
				flags:    n.flags,
				children: n.children,
				pattern:  n.pattern,
			}

			// Update current node to
			// become start of the split
			n.prefix = pattern[:i]
			n.children = []*node{child}
			n.pattern = ""
			n.flags = 0
		}

		// Less than length of pattern,
		// look further down the tree
		if i < len(pattern)-1 {
			// Reslice past common prefix
			pattern = pattern[i:]

			// Get child iter length
			length := len(n.children)
			if n.HasGlobChild() {
				length--
			}

			// Check for child with next prefix char
			for idx := 0; idx < length; idx++ {
				child := n.children[idx]

				// Increment this child and switch to
				if child.prefix[idx] == pattern[0] {
					idx = n.IncrChild(idx)
					n = n.children[idx]
					continue loop
				}
			}
		}

		// Insert child tree for pattern
		n.Insert(pattern, original)
		return
	}
}

// Insert will iterate 'pattern' and create a tree of child nodes as necessary.
func (n *node) Insert(pattern string, original string) {
loop:
	for len(pattern) > 0 {
		// Find next glob instance
		i := indexGlob(pattern)

		switch {
		// No glob found
		case i < 0:
			// Add new prefix child
			child := &node{prefix: pattern}
			n.AddChild(child)
			n = child
			break loop

		// Leading glob
		case i == 0:
			// Get / create glob child
			n = n.GlobChild()

		// Glob in middle of pattern
		case i < len(pattern):
			// Add new prefix child
			child := &node{prefix: pattern[:i]}
			n.AddChild(child)
			n = child

			// Create new glob child
			n = n.GlobChild()
		}

		// Get next pattern part
		pattern = pattern[i+1:]
	}

	// Check for conflicts
	if len(n.pattern) > 0 {
		panic("glob expression conflict")
	}

	// Set match pattern
	n.pattern = original
}

// GlobChild will fetch (creating if needed) glob child for current node.
func (n *node) GlobChild() *node {
	switch {
	case n.IsGlob():
		panic("illegal glob expression")
	case n.HasGlobChild():
		return n.children[len(n.children)-1]
	default:
		child := &node{flags: globBit}
		n.children = append(n.children, child)
		n.flags |= globChildBit
		return child
	}
}

// HasGlobChild returns whether this node has a glob child.
func (n *node) HasGlobChild() bool {
	return (n.flags&globChildBit != 0)
}

// AddChild will add a regular (prefix) child to the current node.
func (n *node) AddChild(child *node) {
	if n.HasGlobChild() {
		// Add child at penultimate place, keep glob child at end
		glob := n.children[len(n.children)-1]
		n.children = append(n.children[:len(n.children)-1], child, glob)
	} else {
		// Simply append child to children slice
		n.children = append(n.children, child)
	}
}

// IncrChild will move the child node at 'idx' up to next priority placement in n.children, returning new idx.
func (n *node) IncrChild(idx int) int {
	newIdx := idx

	// Fetch child at idx's priority
	priority := n.children[idx].Priority()

	// Bump child at idx to next slot, as its priority will incr
	for newIdx > 0 && n.children[newIdx-1].Priority() < priority {
		// Swap node positions
		n.children[newIdx-1], n.children[newIdx] =
			n.children[newIdx], n.children[newIdx-1]

		// Decr
		newIdx--
	}

	return newIdx
}

// Priority returns the current node priority where lower is better.
func (n *node) Priority() int {
	p := len(n.children)
	if n.IsGlob() {
		p++
	}
	return p
}

// isGlob returns whether current node is a glob.
func (n *node) IsGlob() bool {
	return (n.flags&globBit != 0)
}

// indexGlob looks for the first instance of (non delimitted) glob in 's'.
func indexGlob(s string) int {
	i, delim := -1, false
	for idx, c := range s {
		// Delim char
		if c == '\\' {
			delim = !delim
			continue
		}

		// Glob found!
		if c == '*' {
			if !delim {
				i = idx
			}
		}

		// Ensure unset
		delim = false
	}
	return i
}

// longestCommonPrefix finds the longest prefix that both strings share.
func longestCommonPrefix(a, b string) int {
	// Set max length
	max := len(a)
	if len(b) < max {
		max = len(b)
	}

	// Iter while bytes are similar
	i := 0
	for i < max {
		// Find next rune in each
		runeA, l := utf8.DecodeRuneInString(a[i:])
		runeB, _ := utf8.DecodeRuneInString(b[i:])

		// Check that runes match
		if runeA != runeB {
			return i
		}

		// Incr
		i += l
	}

	return i
}

// wife is stinky
